<?php
/**
 * LaunchKey OAuth
 *
 * PHP OAuth Library
 *
 * @author  LaunchKey <developers@launchkey.com>
 * @package LaunchKey
 */

class LaunchKey_OAuth
{

    private $app_key = "";
    private $secret_key = "";
    private $redirect_uri = "";

    /**
     * @param string $app_key
     * @param string $secret_key
     * @param string $redirect_uri
     */
    public function __construct($app_key, $secret_key, $redirect_uri)
    {
        $this->app_key = $app_key;
        $this->secret_key = $secret_key;
        $this->redirect_uri = $redirect_uri;
    }

    /**
     * @param string $code
     *
     * @return mixed
     */
    public function callback($code)
    {
        $data = array();
        $data['client_id'] = $this->app_key;
        $data['client_secret'] = $this->secret_key;
        $data['redirect_uri'] = $this->redirect_uri;
        $data['code'] = $code;
        $data['grant_type'] = "authorization_code";

        $response = $this->json_curl("https://oauth.launchkey.com/access_token", $data);
        return $response;
    }

    /**
     * @param string $color
     * @param string $size
     * @param string $length
     *
     * @return string
     */
    public function login($color = 'blue', $size = 'medium', $length = 'full')
    {
        $login_url
            = "https://oauth.launchkey.com/authorize?client_id=" . $this->app_key . "&redirect_uri="
            . $this->redirect_uri;

        switch ($length) {
            case 'short':
                $text = 'Log in';
                break;
            case 'mini':
                $text = '';
                break;
            default:
                $text = 'Log in with LaunchKey';
                break;
        }
        $login = '<link rel="stylesheet" href="https://launchkey.com/stylesheets/buttons.css">';
        $login
            .= '<a href="' . $login_url . '" title="Log in with LaunchKey" class="lkloginbtn ' . $length . ' ' . $color
            . ' ' . $size . '"><span class="icon"></span><span class="text">' . $text . '</span></a>';
        return $login;
    }

    /**
     * @param string $access_token
     *
     * @return bool
     */
    public function logout($access_token)
    {
        $this->json_curl('https://oauth.launchkey.com/logout', array('access_token' => $access_token));
        return true;
    }

    /**
     * @param string $refresh_token
     *
     * @return array
     */
    public function refresh($refresh_token)
    {
        $data = array();
        $data['client_id'] = $this->app_key;
        $data['client_secret'] = $this->secret_key;
        $data['redirect_uri'] = $this->redirect_uri;
        $data['refresh_token'] = $refresh_token;
        $data['grant_type'] = "refresh_token";

        $refresh_response = $this->json_curl('https://oauth.launchkey.com/access_token', $data);
        return $refresh_response;
    }

    /**
     * @param string $access_token
     *
     * @return bool
     */
    public function verify($access_token)
    {
        $headers = array("Authorization: Bearer " . $access_token);
        $verify_response = $this->json_curl("https://oauth.launchkey.com/resource/ping", array(), "POST", $headers);
        if (count($verify_response) && isset($verify_response['message'])) {
            if ($verify_response['message'] === 'valid') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * PBKDF2 Implementation (described in RFC 2898)
     * credit: http://www.itnewb.com/tutorial/Encrypting-Passwords-with-PHP-for-Storage-Using-the-RSA-PBKDF2-Standard
     *
     * @param string $to_hash - the user returned from LaunchKey to be protected locally
     * @param string $salt - default is provided, but providing a salt-per-hash is the best practice to maximize security
     * @param int    $iteration_count - more than 1000, but don't get crazy!
     * @param string $algo
     *
     * @return string
     */
    public function pbkdf2($to_hash, $salt = 'provide_your_own_salt_per_hash_and_save_both', $iteration_count = 2048, $algo = 'sha256')
    {
        $derived_key = '';
        $key_length = 32;
        $hash_length = strlen(hash($algo, null, true));
        $kb = ceil($key_length / $hash_length); //blocks to compute

        for ($block = 1; $block <= $kb; $block++) {
            // Initial hash for this block
            $ib = $b = hash_hmac($algo, $salt . pack('N', $block), $to_hash, true);
            // Perform block iterations
            for ($i = 1; $i < $iteration_count; $i++) {
                $ib ^= ($b = hash_hmac($algo, $b, $to_hash, true));
            }

            $derived_key .= $ib; // Append iterated block
        }

        return base64_encode(substr($derived_key, 0, $key_length));
    }

    /**
     * @param string $url
     * @param array  $params
     * @param string $method
     * @param array  $headers
     *
     * @return mixed
     */
    private function json_curl($url, $params = array(), $method = 'GET', $headers = array())
    {
        if ($method === 'GET' && count($params)) {
            $query_params = http_build_query($params);
            $url = $url . "/?" . $query_params;
        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, '10');

        if (count($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        if ($method != 'GET') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }

        $result = json_decode(curl_exec($ch), true);
        return $result;
    }
}
